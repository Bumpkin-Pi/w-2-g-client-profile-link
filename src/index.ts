import axios, { AxiosRequestConfig } from "axios";
/**
 * W2GCLient
 */
export class W2GClient {
	apiKey: String;
	roomID: String = "";

	/**
	 * Constructor for a W2G Client
	 *
	 * @param apiKey Your W2G API key
	 */
	constructor(apiKey: String) {
		this.apiKey = apiKey;
	}

	/**
	 * Creates a W2G Room
	 *
	 * @param link Link to a video that will be played
	 * @param color Background color
	 * @param opacity Background opacity#
	 *
	 * @returns Link to the W2G Room
	 */
	async create(link = "", color = "000000", opacity = "80") {
		const config: AxiosRequestConfig = {
			method: "post",
			url: "https://w2g.tv/rooms/create.json",
			params: {
				w2g_api_key: this.apiKey,
				share: link,
				bg_color: color,
				bg_opacity: opacity,
			},
		};

		await axios(config)
			.then((res) => (this.roomID = res.data.streamkey))
			.catch((error) => console.log(error));
	}

	/**
	 * Updates the given room the given video
	 *
	 * @param link Link of a video that should be played
	 * @param roomID ID of the room you want to update
	 */
	async update(link = "", roomID = this.roomID) {
		const config: AxiosRequestConfig = {
			method: "post",
			url: "https://w2g.tv/rooms/" + roomID + "/sync_update",
			params: {
				w2g_api_key: this.apiKey,
				item_url: link,
			},
		};

		await axios(config)
			.then((res) => (this.roomID = res.data.streamkey))
			.catch((error) => console.log(error));
	}

	/**
	 * Returns link to a prior created W2G Room
	 *
	 * @returns URL to the created W2G room
	 */
	getLink(): String {
		var link: String = "https://w2g.tv/rooms/" + this.roomID;

		return link;
	}
}

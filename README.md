# W2G.tv Client

A Client for Interacting with W2G.tv Rooms

# Usage

## Initialising Client

- Api Token found on your [W2G Profile](https://w2g.tv/en/account/edit_user#), under "Tools / API"

`var client = new W2GClient("<api_token>");`

## Create a Room

- Link to a video (optional)
- Backgroundcolor (optional)
- Backgroundopacity (optional)

```
client.create(link?, color?, opacity?);
var link = client.getLink()
```

## Update Room

- Link to a Video
- roomID of the Room that will be update (Optional if a room was prior created using the client)

`client.update(link, roomID?);`

# TODO:

- [x] Creating Rooms using the API
- [x] Upate Rooms
- [ ] Adding Videos/Playlist to Queue
